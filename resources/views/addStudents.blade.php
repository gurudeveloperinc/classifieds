@extends('layouts.app')

@section('content')


<div id="index-banner" class="parallax-container">
    @if( Session::has('success') )
        <div class="green" align="center">{{Session::get('success')}}</div>
    @endif

    <div class="section no-pad-bot">
		<div class="container">
			<br><br>
			<h1 class="header center teal-text text-lighten-2">Welcome {{Auth::user()->fname}}</h1>

			<div class="row center" style="margin-top: -30px">
				<h4 class="subtitile">ID Number: {{Auth::user()->sid}}</h4>
				<h5 class="header col s12 light">Add students to the system</h5>

			</div>
			<div class="row center">
				@if(Auth::user()->isAdmin == 1)

				<a href="{{url('/approve-ads')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Approve Ads</a>

				@endif
				<a href="{{url('/post-ad')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Post Ad</a>
				<a href="{{url('/profile#active')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Published Ads</a>
				<a href="http://materializecss.com/getting-started.html" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Messages</a>
			</div>
			<br><br>

		</div>
	</div>
</div>


<div class="margin" align="center">

    <form style="margin: 40px;" method="post" enctype="multipart/form-data" action="{{url('/add-students')}}">
        {{csrf_field()}}
        <input type="file" name="file" class="input"> <br>
        <button class="btn" type="submit">Add Students</button>
    </form>
    <br>
    <a href="{{url('/uploads/addstudents.xlsx')}}">Download Sample</a>



</div>


@endsection