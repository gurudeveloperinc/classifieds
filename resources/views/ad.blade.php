@extends('layouts.app')

@section('content')

    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h1 class="header center teal-text text-lighten-2">Ad Details</h1>

                <div class="row center white-text">
                    <h5 class="header col s12 light">{{$ad->title}}</h5>
                </div>

                <div class="row center white-text" >
                        Sold by: {{$ad->Student->fname}} ({{$ad->Student->sid}})
                </div>
                <br><br>

            </div>
        </div>
    </div>


    <div class="margin row">

        <div class="col m9">

            <div align="center" style="margin-top: -80px;margin-bottom: -50px;">
                <div class="carousel">
                    @foreach($ad->Images as $item)
                    <a class="carousel-item"><img src="{{$item->url}}"></a>
                    @endforeach
                </div>

                <a class="waves-effect waves-teal btn center-aligns" id="openGallery">Expand Gallery</a>
            </div>


                     <span class="subheader teal-text">Price:</span>
                        <span class="title" style="font-size: 24px"> {{$ad->price}}</span> <span>cedis</span>
                     <h6 class="subheader teal-text">Short Description:</h6>
                        <p class="flow-text">{{$ad->shortDesc}}</p>
                    <hr>
                    @if(!empty($ad->longDesc))
                        <h6 class="subheader teal-text">Detailed Description:</h6>
                        <p class="flow-text">{{$ad->longDesc}}</p>
                    @endif

        </div>


        <div class="col m3" align="center" style="margin-top: 50px;">
            <img src="{{url('images/logo.png')}}" style="width:200px">
            <p>
                <span style="font-size: 24px"> {{$ad->Student->fname}} {{$ad->Student->sname}}</span> <br>
                {{$ad->Student->phone}} <br>
                {{$ad->Student->email}} <br>
                {{$ad->Student->rep}} Star Rep<br>

            <div class="chip" align="center" style="background-color: #B89144; color:white;">
                Interests: {{$ad->interested}}
            </div>

            <br>
                <span style="font-size: 24px; font-weight: 700; color:#01582A;">&#x20B5; {{$ad->price}}</span>
            </p>
            <br>
            <a href="{{url('/send-message/'.  $ad->aid)}}" style="padding:0 10px; background-color: #025A29" class="waves-effect green waves-light btn">
                <i class="material-icons left" style="margin:0;">email</i>
            </a>
            <a class="int waves-effect waves-light btn" style="padding: 0 10px; margin:2px;" data-aid="{{$ad->aid}}" data-uid="{{$ad->Student->uid}}">
                <i class="material-icons left" style="margin: 0">info_outline</i>
            </a>

            <br>
            <a href="{{url('/buy/'. $ad->aid . '/' . $ad->uid)}}" style="background-color: #025A29" class="waves-effect green waves-light btn">Buy</a>


            <br><br>

            <a href="{{url('/profile/' . $ad->Student->sid)}}">View {{$ad->Student->fname}}'s Ads</a> <br>
            <a href="{{url('/')}}">View All Ads</a>
        </div>


    </div>

    {{-- photo swipe gallery --}}
    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
             It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
                PhotoSwipe keeps only 3 of them in the DOM to save memory.
                Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader--active when preloader is running -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>
    {{-- end photo swipe gallery--}}

    <script>

        $(document).ready(function(){
            window.Laravel.data = '<?php echo $images; ?>';
            var imageItems = JSON.parse(window.Laravel.data);

            $('.carousel').carousel();

            console.log(imageItems);

            function openGallery(){

                var pswpElement = document.querySelectorAll('.pswp')[0];

                // build items array
                var items = imageItems;

                // define options (if needed)
                var options = {
                    // optionName: 'option value'
                    // for example:
                    index: 0 // start at first slide
                };

                // Initializes and opens PhotoSwipe
                var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();

            }


            $('#openGallery').on('click',openGallery);


        });

    </script>
@endsection
