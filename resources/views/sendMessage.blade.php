@extends('layouts.app')

@section('content')

    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h1 class="header center teal-text text-lighten-2">Purchase Ad</h1>

                <div class="row center white-text">
                    <h5 class="header col s12 light">{{$ad->title}}</h5>
                </div>

                <div class="row center white-text" >
                    Sold by: {{$ad->Student->fname}} ({{$ad->Student->sid}})
                </div>
                <br><br>

            </div>
        </div>
    </div>


    <div class="margin row">

        <div class="center">
            <h3>Price - {{$ad->price}} cedis</h3>

            <h6 class="subheader teal-text">Short Description:</h6>
            <p class="flow-text">{{$ad->shortDesc}}</p>

            <form method="post" action="{{url('/sendMessage')}}">
                {{csrf_field()}}
                <input id="sender" type="hidden" name="sender" value="{{Auth::user()->uid}}">
                <input id="reciever" type="hidden" name="reciever" value="{{$ad->uid}}">
                <div class="input-field col s12">
                    <textarea id="message" name="message" class="materialize-textarea" data-length="120"></textarea>
                    <label for="messagep">Your message</label>
                </div>

                <a class="btn" id="send">Send</a>
            </form>

            <div class="alert alert-danger hidden" id="error"></div>

        </div>

    </div>
    <script>
        $(document).ready(function(){
            $('#send').on('click',function(){

                $.ajax({
                    url: '<?php echo url('/sendMessage'); ?>' ,
                    method: 'post',
                    data:{sender : $('#sender').val(), reciever: $('#reciever').val(), message: $('#message').val(), _token: '<?php echo csrf_token(); ?>'},
                    success: function(response){
                        if(response == 1){
                            window.location = '<?php echo url('/') ?>';
                        } else {
                            $('#error').text("Sorry an error occurred. Try again.");
                            $('#error').removeClass("hidden");
                        }
                    },
                    error: function(response){
                        console.log(response);
                    }
                });


                setTimeout(window.location = '<?php echo url('/') ?>',1000);

            });
        });
    </script>
@endsection
