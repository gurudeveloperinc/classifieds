@extends('layouts.app')

@section('content')


    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h1 class="header center teal-text text-lighten-2">Welcome {{Auth::user()->fname}}</h1>

                <div class="row center" style="margin-top: -30px">
                    <h4 class="subtitile">ID Number: {{Auth::user()->sid}}</h4>
                    <h5 class="header col s12 light">Advertise or purchase products or services.</h5>

                </div>
                <div class="row center">
                    @if(Auth::user()->isAdmin == 1)

                        <a href="{{url('/approve-ads')}}"  class="btn-large waves-effect waves-light teal lighten-1">Approve Ads</a>
                        <a href="{{url('/add-students')}}" class="btn-large waves-effect waves-light teal lighten-1">Add Students</a>

                    @endif
                        <a href="{{url('/post-ad')}}"  class="btn-large waves-effect waves-light teal lighten-1">Post Ad</a>
                    <a href="{{url('/profile#active')}}" class="btn-large waves-effect waves-light teal lighten-1">Published Ads</a>
                    <a href="{{url('/view-messages')}}"  class="btn-large waves-effect waves-light teal lighten-1">Messages</a>
                </div>
                <br><br>

            </div>
        </div>
    </div>


<div class="margin">
    <div class="row">

       @foreach($ads as $ad)
        <div class="col s12 m3">

                <div class="card">
                <div class="card-image">
                    @if($ad->Images != '[]')
                        <img src="{{$ad->Images->first()->url}}" >
                    @endif

                </div>
                <div class="card-stacked">
                    <span class="card-title">{{$ad->title}}</span>
                    <div class="card-content">
                        <p>{{$ad->shortDesc}}</p>
                    </div>

                    <div class="card-action" align="center">
                        <a class="waves-effect waves-light btn viewButton" href="{{url('/ad/' . $ad->aid)}}">View</a>
                        <a class="int waves-effect waves-light btn viewButtonInfo" data-aid="{{$ad->aid}}" data-uid="{{$ad->Student->uid}}"><i class="material-icons left">info_outline</i></a>


                        <hr>
                        <a  href="{{url('/profile/'. $ad->Student->sid)}}">
                            <div class="chip left">
                                <img src="{{url('images/logo.png')}}" alt="Contact Person">
                                {{$ad->Student->fname}}
                            </div>
                            <span class="right">{{$ad->Category->name}}</span>
                        </a>
                    </div>


                </div>
            </div>

        </div>
        @endforeach

    </div>

</div>

@endsection
