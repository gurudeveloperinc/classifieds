@extends('layouts.app')

@section('content')


    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h1 class="header center teal-text text-lighten-2">Inbox</h1>

                <div class="row center" style="margin-top: -30px">

                    <h5 class="header col s12 light">Select a conversation to reply</h5>

                </div>

                <div class="row center">
                    @if(Auth::user()->isAdmin == 1)

                        <a href="{{url('/approve-ads')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Approve Ads</a>

                    @endif
                    <a href="{{url('/post-ad')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Post Ad</a>
                    <a href="{{url('/profile#active')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Published Ads</a>
                    <a href="{{url('/view-messages')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Messages</a>
                </div>
                <br><br>


            </div>
        </div>
    </div>


    <div class="margin">
        <div class="row">

            <ul class="col s12 m6 l6 collection with-header">
            @foreach($users as $item)

                <li data-uid="{{$item->uid}}" class="collection-item">
                    <div >
                        {{$item->fname}} {{$item->sname}}
                        <a href="{{url('/view-message/' . $item->uid)}}"  class="secondary-content"></a>
                    </div>
                </li>

            @endforeach
            </ul>

            <div class="col sm m6 l6">

                <div class="row">

                    <h5 align="center" id="chatTitle" class="card-title"></h5>
                        <p id="display" style="background-color:white; height: 400px; overflow: scroll">

                        </p>

                        <div class="meta-bar chat">
                            <input style="width:90%" data-sender="{{Auth::user()->uid}}" class="nostyle chat-input" id="message" placeholder="Message..." type="text"> <i id="send" style="color:#4DB6AC" class="material-icons">send</i>
                        </div>


                </div>

            </div>

        </div>

    </div>

    <script>
        $(document).ready(function () {
               var reciever;
               var sender = $('#message').data('sender');
            $('.collection-item').on('click',function(){
                var name = $(this).text();
                reciever = $(this).data("uid");
                var count = $('#display span').length;

                $('.collection-item').removeClass('selectedCollection');
                $(this).addClass('selectedCollection');
                $('#chatTitle').text("Chat with " + name );

                $('#display').html("");

                getMessages(reciever,count);
            });

            $('#send').on('click', function () {
                var message = $('#message').val();
                var sender = $('#message').data("sender");

                sendMessage(sender,reciever,message);

            }); // send message when send is clicked

            $('#message').keypress(function (e) {
                if(e.which == 13) {
                    console.log("enter pressed");
                    var message = $('#message').val();
                    var sender = $('#message').data("sender");

                    sendMessage(sender,reciever,message);

                }
            }); // send message when enter is pressed


            function getMessages(uid, count) {

                console.log(sender);

                    //get messages on page load
                    $.ajax({
                        url: '<?php echo url('/messages'); ?>',
                        method: 'post',
                        data:{'uid': uid, '_token': '{{csrf_token()}}' },

                        success: function (response) {
                            var parsedResponse = JSON.parse(response);
                            var item = parsedResponse.timeline;

                            for (i = count; i < parsedResponse.timeline.length; i++) {

                                if (parsedResponse.timeline[i].sender ==  sender) {


                                    $('#display').append(
                                            "<span class='sent'>(you)" + parsedResponse.timeline[i].message +
                                            " ( " + item[i].created_at + ")" +
                                            "</span><br>"
                                    );


                                } else {
                                    $('#display').append(
                                            "<span class='recieved'>" + parsedResponse.timeline[i].message +
                                            " ( " + item[i].created_at + ")" +
                                            "</span><br>"
                                    );
                                }


                            }

                        },
                        error: function (response) {
                            console.log(response);
                        }
                    });
                }

            function sendMessage(sender, reciever, message){

                $.ajax({
                    url:"<?php echo url('/sendMessage') ?>",
                    method: "post",
                    data:{ sender : sender , reciever : reciever, message: message, '_token': '{{csrf_token()}}'},
                    success: function (response) {

                        $('#message').val("");
                        var count = $('#display span').length;
                        setMessageRead(reciever);

                        getMessages(reciever,count);
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });

            }

            function setMessageRead(uid){
                $.ajax({
                    url:"{{url('/setMessageRead')}}",
                    method: "post",
                    data:{uid: uid, '_token': '{{csrf_token()}}'},
                    success: function (response){
                    },
                    error: function () {

                    }
                });
            }



        })
    </script>

@endsection
