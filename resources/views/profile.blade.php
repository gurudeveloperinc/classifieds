@extends('layouts.app')

@section('content')


    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h1 class="header center teal-text text-lighten-2">{{$student->fname}} {{$student->sname}}</h1>

                <div class="row center" style="margin-top: -30px">
                    <h4 class="subtitile">ID Number: {{$student->sid}}</h4>
                    {{--<h5 class="header col s12 light">Advertise or purchase products or services.</h5>--}}

                </div>

                @if(!Auth::guest())
                    @if(Auth::user()->uid == $student->uid)

                <div class="row profileInfo center" style="margin-bottom: 40px">
                           <span class="card-panel">
                            {{count($pendingPurchases)}} Pending Purchases
                        </span>

                        <span class="card-panel">
                            {{count($completedPurchases)}} Completed Purchases
                        </span>

                </div>
                    @endif
                @endif

                <div class="row profileInfo center">

                        <span class="card-panel">
                            {{count($activeAds)}} Active Ads
                        </span>

                        <span class="card-panel">
                            {{count($soldAds)}} Sold Ads
                        </span>

                        @if(!Auth::guest())
                            @if(Auth::user()->uid == $student->uid)
                                <span class="card-panel">
                                {{count($pendingAds)}} Pending Ads
                                </span>
                            @endif
                        @endif
                        <span class="card-panel">
                            {{$student->rep}} Star Rep
                        </span>



                </div>


                <br><br>

            </div>
        </div>
    </div>


    <div>
        <div class="col s12">
            <ul class="tabs tabs-fixed-width">

                @if(!Auth::guest())
                    @if(Auth::user()->uid == $student->uid)
                        <li class="tab col s3"><a class="active" href="#ads">Ads</a></li>
                        <li class="tab col s3"><a href="#purchases">Purchases</a></li>
                    @endif
                @endif
            </ul>
        </div>


        <div id="ads" class="row height">
            <div class="row">

                <ul class="tabs tabs-fixed-width">


                    @if(Auth::guest())
                        <li class="tab col s3"><a class="active" href="#active" > {{$student->fname}}'s Active Ads</a></li>
                    @else
                        @if(Auth::user()->uid == $student->uid)
                            <li class="tab col s3"><a class="active" href="#active" > Your Active Ads</a></li>
                            <li class="tab col s3"><a href="#sold"> Your Sold Ads</a></li>
                            <li class="tab col s3"><a href="#pending"> Your Pending Ads</a></li>
                        @else
                            <li class="tab col s3"><a href="#active" > {{$student->fname}}'s Active Ads</a></li>
                        @endif
                    @endif
                </ul>
            </div>

            <div id="active" class="col s12">
                <div class="margin">
                    @foreach($activeAds as $ad)
                        <div class="col s12 m3">

                            <div class="card">
                                <div class="card-image">
                                    @if($ad->Images != '[]')
                                        <img src="{{$ad->Images->first()->url}}" >
                                    @endif

                                </div>
                                <div class="card-stacked">
                                    <span class="card-title">{{$ad->title}}</span>
                                    <div class="card-content">
                                        <p>{{$ad->shortDesc}}</p>
                                    </div>

                                    <div class="card-action" align="center">
                                        <a class="waves-effect waves-light btn viewButton" href="{{url('/ad/' . $ad->aid)}}">View</a>
                                        {{--<a class="waves-effect waves-light btn viewButtonInfo"><i class="material-icons left">info_outline</i></a>--}}

                                        <a href="{{url('/delete/'. $ad->aid)}}" class="btn red">Delete</a>
                                    </div>


                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>


            @if(!Auth::guest())
                @if(Auth::user()->uid == $student->uid)

                    <div id="pending" class="col s12">
                        <div class="margin">
                            @foreach($pendingAds as $ad)
                                <div class="col s12 m3">

                                    <div class="card">
                                        <div class="card-image">
                                            @if($ad->Images != '[]')
                                                <img src="{{$ad->Images->first()->url}}" >
                                            @endif

                                        </div>
                                        <div class="card-stacked">
                                            <span class="card-title">{{$ad->title}}</span>
                                            <div class="card-content">
                                                <p>{{$ad->shortDesc}}</p>
                                            </div>

                                            <div class="card-action" align="center">
                                                <a class="waves-effect waves-light btn viewButton" href="{{url('/ad/' . $ad->aid)}}">View</a>
                                                <a class="waves-effect waves-light btn viewButtonInfo"><i class="material-icons left">info_outline</i></a>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div id="sold" class="col s12">
                        <div class="margin">
                            @foreach($soldAds as $ad)
                                <div class="col s12 m3">

                                    <div class="card">
                                        <div class="card-image">
                                            @if($ad->Images != '[]')
                                                <img src="{{$ad->Images->first()->url}}" >
                                            @endif

                                        </div>
                                        <div class="card-stacked">
                                            <span class="card-title">{{$ad->title}}</span>
                                            <div class="card-content">
                                                <p>{{$ad->shortDesc}}</p>
                                            </div>

                                            <div class="card-action" align="center">
                                                <a class="waves-effect waves-light btn viewButton" href="{{url('/ad/' . $ad->aid)}}">View</a>



                                                @if(isset($ad->Purchase->transactionNo) && $ad->Purchase->transactionNo != 0 && $ad->Purchase->isConfirmed == 1)
                                                    <span class="txnNo" style="background-color: #005B2C"> {{$ad->Purchase->transactionNo}}</span>
                                                @endif

                                                @if(isset($ad->Purchase->transactionNo) && $ad->Purchase->transactionNo != 0 && $ad->Purchase->isConfirmed == 0)
                                                    <span class="txnNo" style="background-color: #B89144"> {{$ad->Purchase->transactionNo}}</span>
                                                @endif


                                                <br><br>

                                                @if(isset($ad->Purchase->isConfirmed) && $ad->Purchase->isConfirmed == 0)
                                                <button class="btn paymentConfirmButton" data-pid="{{$ad->Purchase->pid}}">Confirm</button>
                                                @endif
                                                <br>


                                            </div>


                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                @endif
            @endif
        </div>


        @if(!Auth::guest())
            @if(Auth::user()->uid == $student->uid)
        <div id="purchases" class="height">

            <div class="row">

                <ul class="tabs tabs-fixed-width">

                    @if(Auth::guest())

                    @else
                        @if(Auth::user()->uid == $student->uid)
                            <li class="tab col s3"><a class="active" href="#pendingPurchases" > Your Pending Purchases</a></li>
                            <li class="tab col s3"><a href="#completedPurchases"> Your Completed Purchases</a></li>
                        @else
                            <li class="tab col s3"><a href="#active" > {{$student->fname}}'s Active Ads</a></li>
                        @endif
                    @endif

                </ul>
            </div>


            <div id="pendingPurchases" class="row">
                <div class="margin">
                    @foreach($pendingPurchases as $purchase)
                        <div class="col s12 m3">

                            <div class="card">
                                <div class="card-image">
                                    @if($purchase->Ad->Images != '[]')
                                        <img src="{{$purchase->Ad->Images->first()->url}}" >
                                    @endif

                                </div>
                                <div class="card-stacked">
                                    <span class="card-title">{{$purchase->Ad->title}}</span>
                                    <div class="card-content">
                                        <p>{{$purchase->Ad->shortDesc}}</p>
                                    </div>

                                    <div class="card-action" align="center">
                                        <a class="waves-effect waves-light btn viewButton" href="{{url('/ad/' . $purchase->Ad->aid)}}">View</a>
                                        @if($purchase->transactionNo == 0)
                                        <a class="waves-effect waves-light btn payButton" >Pay</a>
                                        @endif
                                        <span class="hidden">
                                            <label>Transaction No:</label>
                                            <input data-aid="{{$purchase->aid}}">
                                            <button class="btn confirmButton">Confirm</button>
                                         </span>

                                        <span class="hidden txnNo" style="background-color: #B89144">3434</span>

                                        @if($purchase->transactionNo != 0)
                                            <span class="txnNo" style="background-color: #B89144"> {{$purchase->transactionNo}}</span>
                                        @endif

                                    </div>


                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div id="completedPurchases" class="row">

                <div class="margin">
                    @foreach($completedPurchases as $purchase)
                        <div class="col s12 m3">

                            <div class="card">
                                <div class="card-image">
                                    @if($purchase->Ad->Images != '[]')
                                        <img src="{{$purchase->Ad->Images->first()->url}}" >
                                    @endif

                                </div>
                                <div class="card-stacked">
                                    <span class="card-title">{{$purchase->Ad->title}}</span>
                                    <div class="card-content">
                                        <p>{{$purchase->Ad->shortDesc}}</p>
                                    </div>

                                    <div class="card-action" align="center">
                                        <a class="waves-effect waves-light btn viewButton" href="{{url('/ad/' . $purchase->Ad->aid)}}">View</a>

                                        @if($purchase->transactionNo != 0)
                                            <span class="txnNo">{{$purchase->transactionNo}}</span>
                                        @endif


                                    </div>


                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>

        </div>
            @endif
        @endif

    </div>

    <script>
        $(document).ready(function(){
            $('.payButton').on('click',function(){
                $(this).next('span').removeClass('hidden');
            });

            $('.confirmButton').on('click',function(){
                var txn = $(this).prev('input').val();
                var aid = $(this).prev('input').data('aid');

                var self = $(this);

                console.log( $(this).prev('input').val() );
                console.log( $(this).prev('input').data('aid') );


                $.ajax({
                    url: baseUrl + '/txn',
                    method: 'post',
                    data: {'_token': window.Laravel.csrfToken, 'aid' : aid, 'txn' : txn },
                    success : function(response){

                        console.log(response);
                        self.parent().prev('.payButton').addClass('hidden');
                        self.parent().addClass('hidden');
                        self.parent().next('span').text(txn);
                        self.parent().next('span').removeClass('hidden');

                    },
                    error : function(textStatus, errorThrown){
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });



            });

            $('.paymentConfirmButton').on('click',function(){
                var self = $(this);

                var pid = $(this).data('pid');

                console.log(pid);
                $.ajax({
                    url: baseUrl + '/confirm-payment',
                    method: 'post',
                    data: {'_token': window.Laravel.csrfToken, 'pid' : pid },
                    success : function(response){

                        self.prev('span').css('background-color','#005B2C');
                        self.addClass('hidden');

                        console.log(response);
                    },
                    error : function(textStatus, errorThrown){
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });



            });




        });
    </script>

@endsection