@extends('layouts.app')

@section('content')


    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">

                <br><br>
                <h1 class="header center teal-text text-lighten-2">Regent Classifieds</h1>
                <div class="row center">
                    <h5 class="header col s12 light">Advertise or purchase products or services.</h5>
                </div>
                <div class="row center">
                    <a href="{{url('/home')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Get Started</a>
                </div>
                <br><br>

            </div>
        </div>
    </div>


    <div class="margin">
        <div class="row">
            @foreach($ads as $ad)
                <div class="col s12 m3">

                    <div class="card">
                        <div class="card-image">
                            @if($ad->Images != '[]')
                                <img src="{{$ad->Images->first()->url}}" >
                            @endif

                        </div>
                        <div class="card-stacked">
                            <span class="card-title">{{$ad->title}}</span>
                            <div class="card-content">
                                <p>{{$ad->shortDesc}}</p>
                            </div>

                            <div class="card-action" align="center">
                                <a class="waves-effect waves-light btn viewButton" href="{{url('/ad/' . $ad->aid)}}">View</a>
                                <a class="waves-effect waves-light btn viewButtonInfo"><i class="material-icons left">info_outline</i></a>
                                <hr>
                                <a  href="{{url('/profile/'. $ad->Student->sid)}}">
                                    <div class="chip left">
                                        <img src="{{url('images/logo.png')}}" alt="Contact Person">
                                        {{$ad->Student->fname}}
                                    </div>
                                </a>
                            </div>


                        </div>
                    </div>
                </div>

            @endforeach

        </div>

    </div>

    <script>
        $(document).ready(function(){
//            $('.carousel').carousel();
        });
    </script>
@endsection
