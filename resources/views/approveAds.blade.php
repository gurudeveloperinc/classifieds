@extends('layouts.app')

@section('content')


    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h1 class="header center teal-text text-lighten-2">Welcome {{Auth::user()->fname}}</h1>

                <div class="row center" style="margin-top: -30px">
                    <h4 class="subtitile">ID Number: {{Auth::user()->sid}}</h4>
                    <h5 class="header col s12 light">There are {{count($pendingAds)}} ads that need approval</h5>

                </div>
                <div class="row center">
                    @if(Auth::user()->isAdmin == 1)

                        <a href="{{url('/approve-ads')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Approve Ads</a>

                    @endif
                    <a href="{{url('/post-ad')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Post Ad</a>
                    <a href="{{url('/profile#active')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Published Ads</a>
                    <a href="http://materializecss.com/getting-started.html" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Messages</a>
                </div>
                <br><br>

            </div>
        </div>
    </div>


    <div class="margin">

        <table class="stripped">
            <tr>
                <th>Title</th>
                <th>Desc</th>
                <th>Price</th>
                <th>Date Created</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>

            @foreach($pendingAds as $item)
                <tr>
                    <td>{{$item->title}}</td>
                    <td>{{$item->shortDesc}}</td>
                    <td> 	&#x20B5; {{$item->price}}</td>
                    <td>{{$item->created_at}}</td>
                    <td>   <a target="_blank" href="{{url('/ad/' . $item->aid )}}" class="btn-large waves-effect waves-light teal lighten-1">View</a></td>
                    <td>   <a href="{{url('/approve/' . $item->aid )}}" class="btn-large waves-effect waves-light green lighten-1">Approve</a></td>
                    <td>   <a href="{{url('/reject/' . $item->aid )}}" class="btn-large waves-effect waves-light red lighten-1">Reject</a></td>
                </tr>
            @endforeach
        </table>


    </div>


    @endsection