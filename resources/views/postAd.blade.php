@extends('layouts.app')

@section('content')
    @if(isset($status) && $status == "successful")

        <div class="card-panel success" align="center">Ad submitted for approval. Thank you!</div>

    @endif

    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h1 class="header center teal-text text-lighten-2">Post an Ad</h1>

                <div id="dropImage" class="row center">
                    <h5 class="header col s12 light">Carefully add all details for your Ad</h5>
                </div>

                <br><br>

            </div>
        </div>
    </div>


    <div class="margin">


        <div class="row">
            <div id="preview" class="postImagePreview"></div>
        </div>

        <div class="row">

            <div class="col m6 offset-m3">



                <form id="adForm" method="post" enctype="multipart/form-data" action="{{url('/post-ad')}}">
                    {{csrf_field()}}

                    <input multiple onchange="readURL(this);" type="file" id="hiddenFile1" class="hidden" name="images[]">


                        <div class="input-field col s12">
                            <input id="title" type="text" class="validate" name="title">
                            <label for="title">Whats the title of your Ad?</label>

                        </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <textarea name="shortDesc" id="shortDesc" class="materialize-textarea" data-length="160" maxlength="160"></textarea>
                            <label for="shortDesc">Brief description of your item for sale</label>
                        </div>
                    </div>

                        <div class="input-field col s12">
                            <textarea id="longDesc" class="materialize-textarea" name="longDesc" data-length="5000"></textarea>
                            <label for="longDesc">Detailed description of the item you are trying to sell</label>
                        </div>

                    <div class="row">
                        <div class="input-field col s6">
                            <input value="₵" id="price" type="number" name="price" class="validate">
                            <label class="active" for="price">Price</label>
                        </div>
                        <p class="right">
                            <input  type="checkbox" id="negotiable" name="negotiable" />
                            <label for="negotiable" style="margin-top: 35px;">Is it negotiable?</label>
                        </p>
                    </div>


                    <div class="input-field col s12">
                        <select name="category">
                            <option value="" disabled selected>Choose an option</option>
                            @foreach($cats as $item)
                            <option value="{{$item->cid}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        <label>Category</label>
                    </div>

                    <div class="row center">
                        <a  id="addImageButton" class="btn-large waves-effect waves-light teal lighten-1">Add Images</a>
                    </div>




                    <button class="btn waves-effect waves-light" type="submit" name="action">Submit Ad
                        <i class="material-icons right">send</i>
                    </button>


                    <button class="btn waves-effect waves-light red right" type="reset" name="action">Reset
                        <i class="material-icons right">clear</i>
                    </button>


                </form>



            </div>
        </div>



    </div>

    <script>


        $('#shortDesc').characterCounter();
        $('select').material_select();

        $('#addImageButton').on('click',function(){
            $('#hiddenFile1').click();
        });

        function readURL(input) {

            for(i=0; i< input.files.length; i++) {


                if (input.files && input.files[i]) {


                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').append('<img src ="' + e.target.result + '">');
                    };

                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

    </script>
@endsection
