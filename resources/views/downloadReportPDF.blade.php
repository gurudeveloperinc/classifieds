<html>
<head>

    <link href="{{url('css/app.css')}}" rel="stylesheet" type="text/css">

</head>
<body>
<div align="center" class="col-md-12 main " >
    <div>
        <img src="{{url('/images/logo.png')}}" style="height: 100px;" class="logo">
        <h3 class="logoHeader" style="font-size: 30px;color:#8B8E45">REGENT CLASSIFIEDS </h3>
    </div>

    <style>
        h3{
            font-size: 30px;
        }
        body{
            background-color: white;

        }
        td,th{
            text-align: center;
        }

    </style>

    <h3 style="color:#8B8E45">SYSTEM REPORT</h3>
    <h3>Generated on: {{\Carbon\Carbon::now()->toDateTimeString()}}</h3>

    <p>There are a total of {{$messages}} messages and {{count($purchases)}} transactions on the system.
        {{count($updated)}} ads were modified today while {{count($created)}} ads were added today</p>

    <h3 style="color:#8B8E45">AD LIST</h3>

    <table class="stripped">
        <tr>
            <th>Title</th>
            <th>Brief Desc</th>
            <th>Price (GHC)</th>
            <th>Category</th>
            <th>Added By</th>
            <th>Date Created</th>
        </tr>

        @foreach($ads as $item)
            <tr>
                <td>{{$item->title}}</td>
                <td>{{$item->shortDesc}}</td>
                <td>  {{$item->price}}</td>
                <td>{{$item->Category->name}}</td>
                <td>{{$item->Student->fname}} {{$item->Student->sname}} ({{$item->Student->sid}})</td>
                <td>{{$item->created_at}}</td>


            </tr>
        @endforeach
    </table>

</div>
</body>
</html>