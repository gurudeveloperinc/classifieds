@extends('layouts.app')

@section('content')

    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h1 class="header center teal-text text-lighten-2">Purchase Ad</h1>

                <div class="row center white-text">
                    <h5 class="header col s12 light">{{$ad->title}}</h5>
                </div>

                <div class="row center white-text" >
                    Sold by: {{$ad->Student->fname}} ({{$ad->Student->sid}})
                </div>
                <br><br>

            </div>
        </div>
    </div>


    <div class="margin row">

        <div class="center">
            <h3>Price - {{$ad->price}} cedis</h3>

            <h6 class="subheader teal-text">Short Description:</h6>
            <p class="flow-text">{{$ad->shortDesc}}</p>

            <div id="buttons">
                <a href="{{url('/ad/' . $ad->aid)}}" class="btn red">Cancel</a>
                <button id="confirm" data-aid="{{$ad->aid}}" data-uid="{{Auth::user()->uid}}" class="btn green">Confirm</button>
            </div>

            <div class="hidden" id="buyInfo">
                <p>
                    <span style="font-size: 24px">Your purchase has been initiated.</span> <br>
                    Please pay to Mtn Mobile Money account number - <span id="mobileMoneyNumber">{{$ad->Student->phone}}</span> <br>
                    And then put in the transaction ID from your profile page when done. <br><br>

                    <a href="{{url('/profile')}}" class="waves-effect waves-light btn" style="background-color: #B69044">Go to profile</a>
                    <a href="{{url('/home')}}" class="waves-effect waves-light btn teal">Go to Homepage</a>
                </p>
            </div>

        </div>

    </div>
    <script>
        $(document).ready(function(){
            $('#confirm').on('click',function(){

                $.ajax({
                    url: baseUrl + '/confirm/' + $(this).data('aid') + '/' + $(this).data('uid')  ,
                    method: 'get',
                    success: function(response){
                        console.log(response);
                    },
                    error: function(response){
                        console.log(response);
                    }
                });

                $('#buttons').addClass('hidden');
                $('#buyInfo').removeClass('hidden');

            });
        });
    </script>
@endsection
