<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/','HomeController@index');
Route::get('/profile/{sid}','HomeController@viewProfile');


// Messaging routes
Route::post('/messages','HomeController@getMessages');
Route::post('/getMessageUsers','HomeController@getMessageUsers');
Route::post('/sendMessage','HomeController@postSendMessage');
Route::post('/setMessageRead','HomeController@setMessageRead');


Route::get('/delete/{aid}','HomeController@deleteAd');
Route::get('/reports','StudentController@reports');
Route::get('/download-report','StudentController@downloadReportPDF');

Route::get('/home', 'StudentController@index');
Route::get('/profile','StudentController@getProfile');
Route::get('/post-ad','StudentController@getPostAd');
Route::get('/ad/{id}','HomeController@getAd');
Route::get('/ad-images/{id}','HomeController@getAdImages');
Route::get('/buy/{aid}/{uid}','StudentController@getBuyAd');
Route::get('/confirm/{aid}/{uid}','StudentController@confirmBuyAd');
Route::get('/approve-ads','StudentController@getApproveAds');
Route::get('/approve/{aid}','StudentController@approveAd');
Route::get('/reject/{aid}','StudentController@rejectAd');
Route::get('/view-messages','StudentController@viewMessages');
Route::get('/send-message/{aid}','StudentController@sendMessage');
Route::get('/add-students','StudentController@getAddStudents');

Route::post('/confirm-payment','StudentController@postConfirmPayment');
Route::post('/txn','StudentController@postTxn');
Route::post('/int','StudentController@postInt');
Route::post('/post-ad','StudentController@postPostAd');
Route::post('/add-students','StudentController@postAddStudents');