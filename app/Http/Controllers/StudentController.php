<?php

namespace App\Http\Controllers;

use App\ad;
use App\category;
use App\image;
use App\purchase;
use App\User;
use App\message;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use PHPExcel_IOFactory;


class StudentController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index() {

		$ads = ad::all()->sortByDesc('aid')->where('isPublished',1)->where('isSold',0)->where("isDeleted",0);

		$ads = $ads->keyBy("aid");

		$input = Input::get('category');


		if(isset($input) && !empty($input)){
			foreach ($ads as $item){
				if( strtolower($item->Category->name)  != strtolower($input)){
					$ads->forget($item->aid);
				}
			}


		}

		return view('home', ['ads' => $ads]);
	}

	public function reports() {
		$ads = ad::all()->sortByDesc('updated_at')->where("updated_at",">", Carbon::now()->subDay(1));

		return view('reports',['ads' => $ads]);

	}

	public function getReport() {
		$updated = ad::all()->sortByDesc('updated_at')->where("updated_at",">", Carbon::now()->subDay(1));
		$added = ad::all()->where("created_at",">", Carbon::now()->subDay(1));
		$ads = ad::all();
		$messages = message::all()->count();
		$purchaes = purchase::all();

		return view('downloadReportPDF',[
			'updated' => $updated,
			'ads' => $ads,
			'messages' => $messages,
			'purchases' => $purchaes,
			'created' => $added
		]);
	}

	public function downloadReportPDF() {
		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->getReport() );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'landscape' );
			$dompdf->set_option('isRemoteEnabled', 'true');


			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream("Regent_Classifieds_Report_" . Carbon::now()->toDateString());
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}

	}

	public function getProfile(){
		$student = User::where('sid',Auth::user()->sid)->get()->first();

		$activeAds = ad::where('uid', Auth::user()->uid)->where('isPublished',1)->where('isSold',0)->get();
		$pendingAds = ad::where('uid', Auth::user()->uid)->where('isPublished',0)->get();
		$soldAds = ad::where('uid',Auth::user()->uid)->where('isSold',1)->get();

		$pendingPurchases = purchase::where('uid',Auth::user()->uid)->where('isConfirmed',0)->get();
		$completePurchases = purchase::where('uid',Auth::user()->uid)->where('isConfirmed',1)->get();
		return view('profile',[
			'student' => $student,
			'pendingAds' =>$pendingAds,
			'activeAds' => $activeAds,
			'soldAds' => $soldAds,
			'pendingPurchases' => $pendingPurchases,
			'completedPurchases' => $completePurchases
		]);
	}

	public function getPostAd() {
		$cats = category::all();
		return view('postAd', ['cats' => $cats]);
	}

	public function getAd($id){
		$ad = ad::find($id);

		$images = $ad->Images;
		$requiredImages = array();
		foreach($images as $item){
			$image = ['src' => $item->url,'w' =>1200, 'h' =>900  ];
			array_push($requiredImages,$image);
		}

		$requiredImages = json_encode($requiredImages,JSON_UNESCAPED_SLASHES);

		return view('ad',[
			'ad' => $ad,
			'images' => $requiredImages
		]);
	}

	public function getBuyAd($aid,$uid){
		$ad = ad::find($aid);
		$student = User::find($uid);

		return view('buyAd', [
			'student' => $student,
			'ad' => $ad
		]);
	}

	public function getApproveAds() {

		if(Auth::user()->isAdmin == 0) return redirect('/home');

		$pendingAds = ad::all()->sortByDesc("aid")->where("isPublished",0)->where("isDeleted",0);

		return view('approveAds' , ['pendingAds' => $pendingAds]);
	}

	public function getMessageUsers($uid){
		$user = $uid;
		$users = array();
		$response = array();

		$queryResults =message::hydrateRaw("SELECT * FROM messages where sender = '$user' or reciever = '$user' ") ;

		foreach( $queryResults as $row) {

			if($row['sender'] != $user)
				array_push($users,$row['sender']);

			if($row['reciever'] != $user)
				array_push($users,$row['reciever']);
		}

		$users = array_unique($users);

		$usersForQuery = $this->makeAnArray($users);

		$response = User::hydrateRaw("SELECT * FROM users where uid in $usersForQuery ") ;


		if(!empty($response)){
			return $response;
		} else {
			return 0;
		}


	}

	public function getAddStudents() {
		return view('addStudents');
	}



	public function viewMessages(){
		$uid = Auth::user()->uid;
		$users = $this->getMessageUsers($uid);


		return view('viewMessages',[
			'users' => $users
		]);
	}

	public function approveAd( $aid ) {
		if(Auth::user()->isAdmin){
			$ad = ad::find($aid);
			$ad->isPublished = 1;
			$ad->save();

			mail($ad->Student->email,
				"You ad has been approved",
				"Hey ". $ad->Student->fname .", \n\nYour ad - '". $ad->title . "' has been approved. \n \n Thank you for using Regent Classifieds.");
			return redirect('/approve-ads');
		} else {
			return redirect('/home');
		}
	}

	public function rejectAd( $aid ) {
		if(Auth::user()->isAdmin){
			$ad = ad::find($aid);
			$ad->isDeleted = 1;
			$ad->save();

			mail($ad->Student->email,
				"You ad has been rejected",
				" Hey" . $ad->Student->fname . ", \nUnfortunately your ad - '". $ad->title . "' has been rejected by an admin and removed from the Regent Classifieds system. \n".
				"Please read our guidelines for ad posting and try and publish an ad again. \n
				\n Thank you for using Regent Classifieds.");
			return redirect('/approve-ads');
		} else {
			return redirect('/home');
		}
	}


	public function confirmBuyAd($aid,$uid){
		$purchase = new purchase();
		$purchase->aid = $aid;
		$purchase->uid = $uid;
		$purchase->save();

		//increase rep

		$user = User::find(Auth::user()->uid);
		$user->rep = $user->rep + 2;
		$user->save();

		return response("1");
	}

	public function sendMessage( $aid) {
		$ad = ad::find($aid);

		$images = $ad->Images;
		$requiredImages = array();
		foreach($images as $item){
			$image = ['src' => $item->url,'w' =>1200, 'h' =>900  ];
			array_push($requiredImages,$image);
		}

		$requiredImages = json_encode($requiredImages,JSON_UNESCAPED_SLASHES);

		return view('sendMessage',[
			'ad' => $ad,
			'images' => $requiredImages
		]);

	}

	public function postTxn(Request $request){

		$purchase = purchase::where('aid',$request->input('aid'))->first();

		$purchase->transactionNo = $request->input('txn');
		$purchase->save();

		$ad = ad::find($request->input('aid'));
		$ad->isSold = 1;
		$ad->save();

		echo 1;
	}

	public function postConfirmPayment(Request $request){
		$purchase = purchase::find($request->input('pid'));
		$purchase->isConfirmed = 1;
		$purchase->save();

		echo 1;
	}

	public function postInt(Request $request){
		$student = User::find($request->input('uid'));
		$ad = ad::find($request->input('aid'));

		$subject ="Potential buyer for your Regent Ad - " . $ad->title;
		$message = 'Hello ' . $ad->Student->fname .
		           '\n You have a potential buyer for your ad - "'. $ad->title .'" \n
		           contact the student with ID number - ' . $student->sid . ' \n Thanks,
		           \n Regent Classifieds';

		$ad->interested = $ad->interested +1;
		$ad->save();

		mail($ad->Student->email,$subject, $message );
		return redirect('/home');
	}

	public function postPostAd(Request $request){


		$ad = new ad();
		$ad->title = $request->input('title');
		$ad->shortDesc = $request->input('shortDesc');
		$ad->longDesc = $request->input('longDesc');
		$ad->price = $request->input('price');
		$ad->uid = Auth::user()->uid;
		$ad->cid = $request->input('category');
		$ad->save();

		foreach($request->file('images') as $item){
			$inputFileName = $item->getClientOriginalName();
			$item->move("userUploads",$inputFileName);

			$image = new image();
			$image->url = url('userUploads/' . $inputFileName);
			$image->aid = $ad->aid;;
			$image->save();
		}

		$cats = category::all();


		return view('postAd',['status' => 'successful','cats' => $cats]);
	}

	public function postAddStudents( Request $request ) {

		try {


			$inputFileName = $request->file('file')->getClientOriginalName();
			$request->file('file')->move("uploads/",$inputFileName);

			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd()."/" . "uploads/".$inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/" . "uploads/".$inputFileName);

			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				NULL, TRUE, FALSE);


			$count=1;


			// add results to data base from file
			foreach($rowData as $cell){


				$password = Str::random(5);

				$student = new User();
				$student->fname = $cell[0];
				$student->sname = $cell[1];
				$student->email = $cell[2];
				$student->password =  $password;
				$student->sid = $cell[3];
				$student->dept = $cell[4];
				$student->level = "$cell[5]";
				$student->prog = $cell[6];
				$student->nationality = $cell[7];
				$student->gender =  $cell[8];
				$student->phone = $cell[9];
				$student->save();

				mail($cell[4],"Your Regent Classifieds credentials", "You have successfully been added to the system, please login with \nEmail: $cell[2]\nPassword: $password.\nThank you" );


			}

			$request->session()->flash('success','Students added');

			return redirect('/add-students');

		}
		catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
			    . '": ' . $e->getMessage());
		}


	}

	function makeAnArray($data){
		$appIds = "";

		foreach ($data as $item){
			$appIds .= $item .',';
		}
		$appIds .= "0";
		$appIdsInBracket = "(".$appIds.")";
		return $appIdsInBracket;

	}

}
