<?php

namespace App\Http\Controllers;

use App\ad;
use App\message;
use App\purchase;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{

    public function index()
    {
	    if(!Auth::guest()) return redirect('/home');

	    $ads = ad::all()->sortByDesc('aid')->where('isPublished',1)->where('isSold',0);
	    $ads = $ads->keyBy("aid");

	    $input = Input::get('category');


	    if(isset($input) && !empty($input)){
		    foreach ($ads as $item){
			    if( strtolower($item->Category->name)  != strtolower($input)){
				    $ads->forget($item->aid);
			    }
		    }


	    }


        return view('welcome',[
	        'ads' => $ads
        ]);
    }


	public function getAdImages($id){
		$ad = ad::find($id);
		$images = $ad->Images;
		$requiredImages = array();
		foreach($images as $item){
			$image = ['src' => $item->url,'w' =>1200, 'h' =>900  ];
			array_push($requiredImages,$image);
		}

		echo json_encode($requiredImages,JSON_UNESCAPED_SLASHES);
	}

	public function getAd($id){
		$ad = ad::find($id);

		$images = $ad->Images;
		$requiredImages = array();
		foreach($images as $item){
			$image = ['src' => $item->url,'w' =>1200, 'h' =>900  ];
			array_push($requiredImages,$image);
		}


		$requiredImages = json_encode($requiredImages,JSON_UNESCAPED_SLASHES);

		return view('ad',[
			'ad' => $ad,
			'images' => $requiredImages
		]);
	}

	public function deleteAd( Request $request, $aid ) {
		ad::destroy($aid);
		$request->session()->flash("success","Ad deleted");
		return redirect('/profile');
	}

	public function viewProfile($sid) {

		$student = User::where('sid',$sid)->first();

		$uid = $student->uid;

		$activeAds = ad::where('uid', $uid)->where('isPublished',1)->where('isSold',0)->get();
		$pendingAds = ad::where('uid', $uid)->where('isPublished',0)->get();
		$soldAds = ad::where('uid',$uid)->where('isSold',1)->get();

		$pendingPurchases = purchase::where('uid',$uid)->where('isConfirmed',0)->get();
		$completePurchases = purchase::where('uid',$uid)->where('isConfirmed',1)->get();

		return view('profile',[
			'student' => $student,
			'pendingAds' =>$pendingAds,
			'activeAds' => $activeAds,
			'soldAds' => $soldAds,
			'pendingPurchases' => $pendingPurchases,
			'completedPurchases' => $completePurchases
		]);
	}

	public function getMessages(Request $request) {
		$user = Auth::user()->uid;
		$sender = $request->input('uid');

		$messages = message::hydrateRaw("SELECT * FROM messages m where reciever in ( '$user','$sender')  and sender in ( '$user', '$sender') order by created_at");

		$response = ['timeline' => $messages];
		echo json_encode($response);
	}

	public function getMessageUsers(Request $request){
		$user = Auth::user()->uid;
		$users = array();
		$response = array();

		$queryResults =message::hydrateRaw("SELECT * FROM messages where sender = '$user' or reciever = '$user' ") ;

		foreach( $queryResults as $row) {

			if($row['sender'] != $user)
				array_push($users,$row['sender']);

			if($row['reciever'] != $user)
				array_push($users,$row['reciever']);
		}

		$users = array_unique($users);

		$usersForQuery = $this->makeAnArray($users);

		$queryResults = User::hydrateRaw("SELECT * FROM users where uid in $usersForQuery ") ;


		foreach($queryResults as $row){

			$user = array();
			$user ['name'] = $row['name'];
			$user ['uid'] = $row['uid'];
			array_push($response, $user);
		}


		if(!empty($response)){
			echo json_encode($response);
		} else {
			echo 0;
		}


	}

	public function postSendMessage(Request $request){
		$message = $request->input('message');
		$sender = Auth::user()->uid;
		$reciever = $request->input('reciever');

		if(isset($message)  && isset($sender) && isset($reciever)){
			$status = message::hydrateRaw("insert into messages (sender, reciever, message) values ('$sender','$reciever','$message')");


			if( $status ){

				echo "1";

			} else {

				echo "0";

			}
		}

	}

	public function setMessageRead(Request $request){
		$user = Auth::user()->uid;

		$queryResult = message::hydrateRaw("update messages set isRead = 1 where reciever = '$user' and isRead = 0");

		if($queryResult)
			echo 1;
	}


	function makeAnArray($data){
		$appIds = "";

		foreach ($data as $item){
			$appIds .= $item .',';
		}
		$appIds .= "0";
		$appIdsInBracket = "(".$appIds.")";
		return $appIdsInBracket;

	}
}
