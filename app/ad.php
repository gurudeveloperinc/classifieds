<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ad extends Model
{
    protected $primaryKey = 'aid';

	public function Images(){
		return $this->hasMany('App\image','aid','aid');
	}

	public function Student(){
		return $this->hasOne('App\User','uid','uid');
	}

	public function Purchase(){
		return $this->hasOne('App\purchase','aid','aid');
	}

	public function Category(){
		return $this->belongsTo('App\category','cid','cid');
	}
}
